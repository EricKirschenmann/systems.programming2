;Eric Kirschenmann
;February 24, 2015
TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data
	
	;receives: the OFFSET of a string into edx and the size of the string into ecx
	;returns: the signed integer value into eax


	;instantiate a string containing the number to be parsed
	integr BYTE "-2147483648"
	;get the number of elements in the string
	buffer = lengthof integr

	line Byte "Eric Kirschenmann Quiz 1 ParseInteger32", 0

.code
main PROC
    call Clrscr

	mov edx, OFFSET line
	call WriteString
	call Crlf

	;move the string into edx
	mov edx, OFFSET integr
	;move the size into ecx
	mov ecx, buffer
	;call the ParseDecimal32 Procedure
	call ParseInteger32
	;call the WriteDec procedure to display the value
	call WriteInt
	call Crlf

	call DumpRegs
	call Crlf
	exit
main ENDP

END main