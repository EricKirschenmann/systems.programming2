;Eric Kirschenmann
;February 24, 2015
TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data
	;recieves: the OFFSET of the string into edx and the length of the string in ecx
	;returns: the unsigned decimal value into eax

	;instantiate a string containing the number to be parsed
	decimal BYTE "4294967294"
	;get the number of elements in the string
	buffer = lengthof decimal

	line BYTE "Eric Kirschenmann Quiz 1 ParseDecimal32", 0

.code
main PROC
    call Clrscr

	mov edx, OFFSET line
	call WriteString
	call Crlf

	;move the string into edx
	mov edx, OFFSET decimal
	;move the size into ecx
	mov ecx, buffer
	;call the ParseDecimal32 Procedure
	call ParseDecimal32
	;call the WriteDec procedure to display the value
	call WriteDec
	call Crlf

	call DumpRegs
	call Crlf
	exit
main ENDP

END main