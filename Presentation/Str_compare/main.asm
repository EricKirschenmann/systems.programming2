;Eric Kirschenmann
;February 24, 2015
TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data

	comment #
		Possible outputs:
							CF	ZF
		string1 < string2	1	0
		string1 = string2	0	1
		string1 > string2	0	0

		Str_compare is found on pg. 338 in the book.
	#

	;recieves: two strings using invoke
	;returns: nothing but changes the Carry Flag and the Zero Flag

	;instantiate strings to be compared
	string1 BYTE "string", 0 ;value = 0x297
	string2 BYTE "String", 0 ;value = 0x277
	string3 BYTE "strin ", 0 ;value = 0x250

	;instantiate string containing example results
	line1 BYTE "0x250 < 0x297, CF = 1 and ZF = 0:", 0
	line2 BYTE "0x297 = 0x297, CF = 0 and ZF = 1:", 0
	line3 BYTE "0x297 > 0x277, CF = 0 and ZF = 0:", 0

	line BYTE "Eric Kirschenmann Quiz 1 Str_compare", 0

.code
main PROC
    call Clrscr
	
	mov edx, OFFSET line
	call WriteString
	call Crlf

	;print the expected output
	mov edx, OFFSET line1
	call WriteString

	;since 0x250 < 0x297, CF = 1 and ZF = 0
	invoke Str_compare, ADDR string3, ADDR string1
	call DumpRegs

	;print the expected output
	mov edx, OFFSET line2
	call WriteString

	;since 0x297 = 0x297, CF = 0 and ZF = 1
	invoke Str_compare, ADDR string1, ADDR string1
	call DumpRegs

	;print the expected output
	mov edx, OFFSET line3
	call WriteString

	;since 0x297 > 0x277, CF = 0 and ZF = 0
	invoke Str_compare, ADDR string1, ADDR string2
	call DumpRegs

	call Crlf
	exit
main ENDP

END main