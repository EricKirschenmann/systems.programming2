;Eric Kirschenmann
;February 26, 2015
Title Fibonacci Print            (main.asm)

INCLUDE Irvine32.inc

.data
	;name, project, and question number
	line BYTE "Eric Kirschenmann, Project 2, Question 2", 0
	;starting values of the array
	fibArray BYTE 1, 1

.code
main PROC
	call Clrscr

	;print name and question number
	mov edx, OFFSET line
	call WriteString
	call Crlf

	;print first two elements outside of the loop
	mov eax, 0
	mov al, fibArray
	call WriteDec
	call Crlf

	mov al, [fibArray + TYPE fibArray]
	call WriteDec
	call Crlf


	;clear registers
	mov eax, 0
	mov ebx, 0
	mov edx, 0
	;store the first two values
	mov bl, fibArray
	mov dl, [fibArray + TYPE fibArray]

	;start loop to go however many iterations
	mov ecx, 30
fib:
	;start adding the numbers together
	mov eax, edx
	add eax, ebx
	
	;print out the current fib number
	call WriteDec
	call Crlf

	;shift the current and previous numbers
	;into the other registers
	mov ebx, edx
	mov edx, eax

	loop fib



	call DumpRegs
   exit
main ENDP
END main