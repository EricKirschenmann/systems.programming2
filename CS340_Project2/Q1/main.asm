;Eric Kirschenmann
;February 26, 2015
Title Flag Set             (main.asm)

INCLUDE Irvine32.inc

.data

	;name, project, and question number
	line BYTE "Eric Kirschenmann, Project 2, Question 1", 0
	;strings that provide information on why flags are changed
	cFlag BYTE "0xFFFFFFFF + 0x1 causes a carry, CF set", 0
	oFlag BYTE "-32,768 - 1 is larger than the 16-bit register can hold, OF set", 0
	zFlag BYTE "0x1000 - 0x1000 = 0 so ZF is set", 0
	sFlag BYTE "0 - 1 is negative so SF is set", 0

.code
main PROC
	call Clrscr

	;print name and question number
	mov edx, OFFSET line
	call WriteString
	call Crlf

	;print explanation
	mov edx, OFFSET cFlag
	call WriteString
	mov eax, 0
	;Carry Flag
	mov eax, 0FFFFFFFFh
	add eax, 1h
	call DumpRegs

	;print explanation
	mov edx, OFFSET oFlag
	call WriteString
	mov eax, 0
	;Overflow Flag
	mov ax, -32768
	sub ax, 1
	call DumpRegs

	;print explanation
	mov edx, OFFSET zFlag
	call WriteString
	mov eax, 0
	;Zero Flag
	mov eax, 1000h
	sub eax, 1000h
	call DumpRegs

	;print explanation
	mov edx, OFFSET sFlag
	call WriteString
	;Sign Flag
	mov eax, 0
	sub eax, 1
	call DumpRegs

   exit
main ENDP
END main