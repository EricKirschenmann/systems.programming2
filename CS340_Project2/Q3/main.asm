Title Array Flip            (main.asm)

INCLUDE Irvine32.inc

.data

	line BYTE "Eric Kirschenmann, Project 2, Question 3", 0

	array BYTE 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
	endItem DWORD ?


.code
main PROC
	call Clrscr

	;print name and question number
	mov edx, OFFSET line
	call WriteString
	call Crlf

	mov eax, 0
	mov esi, OFFSET array

	;Get address of last element
	mov ecx, lengthof array - 1
l1: 
	add esi, TYPE array
	loop l1

	;save address of last and first elements
	mov endItem, esi
	mov esi, OFFSET array

	;print the array
	mov ecx, lengthof array
l2:
	mov eax, 0
	mov al, [esi]
	call WriteInt
	call Crlf
	add esi, TYPE array
	
	loop l2

	;reset esi with address of first element
	mov esi, OFFSET array

	;swap elements in array
	mov ecx, lengthof array /2
l3:
	mov eax, 0
	mov ebx, 0
	mov edx, 0

	mov ebx, endItem
	mov dl, [ebx]
	mov al, [esi]

	;call dumpregs

	xchg eax, edx
	
	mov [ebx], dl
	mov [esi], al

	sub endItem, TYPE array
	add esi, TYPE array

	loop l3

	mov esi, OFFSET array

	;print the array again but hopefully in reverse
	mov ecx, lengthof array
l4:
	mov eax, 0
	mov al, [esi]
	call WriteInt
	call Crlf
	add esi, TYPE array
	
	loop l4


	call DumpRegs

   exit
main ENDP
END main