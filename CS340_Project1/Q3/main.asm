;Eric Kirschenmann
;Project 1
;Problem 3
TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data
	;create symbolic constants for the days of the week
	Sun = "Sun"
	Mon = "Mon"
	Tues = "Tues"
	Wed = "Wed"
	Thurs = "Thur"
	Fri = "Fri"
	Sat = "Sat"

	;create an array instantiated with the symbolic constants
	;there is a problem with how it saves, as it outputs in reverse, i.e. "nuS"
	;I don't know why it is storing in reverse
	daysOfWeek DWORD Sun, Mon, Tues, Wed, Thurs, Fri, Sat
	

	;String containing problem information
	line BYTE "Eric Kirschenmann, Project 1, Question 3", 0
	

.code
main PROC
	call Clrscr

	;print first value of array
	mov edx, OFFSET daysOfWeek
	call WriteString
	call Crlf

	;print string containing problem information
	mov edx, OFFSET line
	call WriteString
	call Crlf

	exit
main ENDP

END main