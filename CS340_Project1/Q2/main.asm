;Eric Kirschenmann
;Project 1
;Problem 2
TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data
	
	;all variable types with data that fits the data type
	aByte BYTE 10
	bByte SBYTE -10
	mWord WORD 2000
	nWord SWORD -2455
	hWord DWORD	0FFFh
	yWord SDWORD -100000
	xWord FWORD 280000000000000
	zWord QWORD 18000000000000000000
	mReal REAL4 -1.2
	nReal REAL8 3.2E-260
	oReal REAL10 4.6E+4096

	;String that contains the information for this question
	line BYTE "Eric Kirschenmann, Project 1, Question 2", 0
	

.code
main PROC
	call Clrscr

	;print the String with the problem information
	mov edx, OFFSET line
	call WriteString
	call Crlf

	exit
main ENDP

END main