;Eric Kirschenmann
;Project 1
;Problem 1
TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data
	;Declare 3 16-bit variables to subtract
	num1 word 100
	num2 word  60
	num3 word  20

	;string that contains information about the question
	line byte "Eric Kirschenmann, Project 1, Question 1", 0

.code
main PROC
    call Clrscr

	;Print out the string containing the information
	mov edx, OFFSET line
	call WriteString
	call Crlf

	;clear out eax, so there is no high-order data messing up the output
    mov eax, 0
	;move the first number to the ax register
    mov ax, num1
	;subtract the second number
    sub ax, num2
	;subtract the third number
    sub ax, num3
	;print the the eax register
    call WriteInt
	;dump the registers
    call dumpRegs

	exit
main ENDP

END main