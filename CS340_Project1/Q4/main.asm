TITLE MASM Template						(main.asm)

INCLUDE Irvine32.inc
.data

	line BYTE "Eric Kirschenmann, Project 1, Question 4", 0

	array DWORD 1, 2, 3, 4, 5
	

.code
main PROC
	call Clrscr

	mov edx, OFFSET line
	call WriteString
	call Crlf

	mov eax, array
	call WriteInt
	call Crlf

	mov eax, [array + 4]
	call WriteInt
	call Crlf

	mov eax, [array + 8]
	call WriteInt
	call Crlf

	mov eax, [array + 12]
	call WriteInt
	call Crlf

	mov eax, [array + 16]
	call WriteInt
	call Crlf


	exit
main ENDP

END main